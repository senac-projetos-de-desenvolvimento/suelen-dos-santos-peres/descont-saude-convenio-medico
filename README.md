## Como iniciar:

Certifique-se que tenha instalado os seguintes pacotes:
php
```sh
apt-get install php-pear php7.2-curl php7.2-dev php7.2-gd php7.2-mbstring php7.2-zip php7.2-mysql php7.2-xml
```

composer
```sh
php -r "copy ('https://getcomposer.org/installer', 'composer-setup.php');"
```

laravel
```sh
composer create-project laravel/laravel example-app
```

mysql
```sh
instalar Xampp ou MySQL Workbench
```

Após clonar o projeto, fazer atualização dos pacotes:
```sh
comṕoser update
```

Criar a tabela no phpmyadmin ou no MySQL Workbench e rodar as migrations:
```sh
php artisan migrate
```

Depois rodar as seeds
```sh
php artisan db:seeds
```

Startar o server
```sh
php artisan server
```
